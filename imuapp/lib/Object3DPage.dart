// ignore: file_names
import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
// ignore: avoid_web_libraries_in_flutter
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:math' as math;
// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_3d_obj/flutter_3d_obj.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class Object3DPage extends StatefulWidget {
  final BluetoothDevice server;
  const Object3DPage({required this.server});

  @override
  _Object3DPage createState() => _Object3DPage();
}

class _Object3DPage extends State<Object3DPage>
    with SingleTickerProviderStateMixin {
  late BluetoothConnection connection;
  bool isConnecting = true;
  bool get isConnected => connection != null && connection.isConnected;

  bool isDisconnecting = false;
  final angle = 30 / 180 * math.pi;
  //Object3D board3D;
  var onDragPositionStart;
  late double angleXInit;
  late double angleX;
  late double angleY;
  late double angleZ;
  late var angleXtest = 0.0;
  late var angleYtest = 0.0;
  late var angleZtest = 0.0;
  late double scaletest;
  late String bufferX;
  late String bufferY;
  late String bufferZ;
  bool activeX = false;
  bool activeY = false;
  bool activeZ = false;
  // ignore: non_constant_identifier_names
  bool activeX_Prev = false;
  // ignore: non_constant_identifier_names
  bool activeY_Prev = false;
  // ignore: non_constant_identifier_names
  bool activeZ_Prev = false;
  // ignore: prefer_typing_uninitialized_variables
  late int dataRecev;
  late Timer _everySecond;
  late String _now;
  //final Object board = Object(fileName: 'assets/boardfromblender_v2.obj');
  @override
  void initState() {
    scaletest = 6.0; //6.0
    angleXInit = 0.0;
    angleX = angleXInit;
    angleY = 0.0;
    angleZ = 0.0;

    super.initState();
    BluetoothConnection.toAddress(widget.server.address).then((_connection) {
      print('Connected to the device');
      connection = _connection;
      setState(() {
        isConnecting = false;
        isDisconnecting = false;
      });
      _everySecond =
          Timer.periodic(const Duration(milliseconds: 50), (Timer t) {
        setState(() {
          _now = DateTime.now().second.toString();
          /*Configuration Board 3D */
          angleX = -1 * angleYtest; //angleYtest PITCH
          angleZ = angleXtest; //Roll
          angleY = angleZtest; //yaw
          /*Configuration Sword */
          /*angleX = -1 * angleYtest; //angleYtest PITCH
          angleZ = angleXtest; //Roll
          angleY = 180 - angleZtest; */ //yaw
        });
      });
    }).catchError((error) {
      print('Cannot connect, exception occured');
      print(error);
    });
  }

  @override
  void dispose() {
    // Avoid memory leak (`setState` after dispose) and disconnect
    if (isConnected) {
      isDisconnecting = true;
      connection.dispose();
      connection = null!;
    }

    super.dispose();
  }

  readData() {
    // ignore: unnecessary_null_comparison

    connection.input!.listen((Uint8List data) {
      //dataRecev = ascii.decode(data);

      print("Frame: $data");
      print(data.length);

      if (data.length > 5) {
        dataRecev = data[1];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleXtest = dataRecev.toDouble();

        print("x: $angleXtest");

        dataRecev = data[3];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleYtest = dataRecev.toDouble();
        print("y: $dataRecev");

        dataRecev = data[5];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleZtest = dataRecev.toDouble();
        print("z: $dataRecev");
      } else if ((data.length > 3) && (data.length < 6)) {
        dataRecev = data[1];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleXtest = dataRecev.toDouble();

        print("x: $angleXtest");

        dataRecev = data[3];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleYtest = dataRecev.toDouble();
        print("y: $dataRecev");

        //angleZtest = 0.0;
        //print("z: 0");
      } else if (data.length == 3) {
        dataRecev = data[1];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleXtest = dataRecev.toDouble();
        print("x: $dataRecev");

        angleYtest = 0.0;
        print("y: 0");
        //angleZtest = 0.0;
        //print("z: 0");
      } else if (data.length == 2) {
        dataRecev = data[1];
        if (dataRecev > 127) {
          dataRecev = (dataRecev - 1) - 0xFF;
        }
        angleXtest = dataRecev.toDouble();
        print("x: $dataRecev");
        angleYtest = 0.0;
        print("y: 0");
      } else if (data.length == 1) {
        print("x: 0");

        print("y: 0");
        angleXtest = 0.0;
        //angleYtest = 0.0;

      }

      //connection.output.add(data); // Sending data
    }).onDone(() {
      print('Disconnected by remote request');
    });
    //connection.output.add(utf8.encode(data + "\r\n"));
  }

  void _incrementCounter() {
    setState(() {
      readData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Object 3D"),
      ),
      body: Center(
        child: Object3D(
          size: const Size(300, 300),
          asset: true,
          //path: 'assets/Sting-Sword-lowpoly.obj',
          path: 'assets/Board_From_Blender_Final.obj',

          angleX: angleX,
          angleY: angleY,
          angleZ: angleZ,
          zoom: scaletest,
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.bluetooth),
      ),

      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
