import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:flutter_3d_obj/flutter_3d_obj.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter 3D Demo',
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Flutter 3D"),
        ),
        body: Center(
          child: Object3D(
            size: const Size(400.0, 400.0),
            path: "assets/astronaut-obj/astronaut.obj",
            asset: true,
          ),
        ),
      ),
    );
  }
}
