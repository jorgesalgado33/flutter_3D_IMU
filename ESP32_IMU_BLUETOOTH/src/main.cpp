/**
 * @file main.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief main class
 * @version 
 * @date 13-06-2020
 * 
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDES - ARDUINO
 *******************************************************************************************************************************************/
#include <Arduino.h>
#include "../lib//Definition/GlobalDef.h"
#include "soc/rtc_wdt.h"
/*******************************************************************************************************************************************
 *  												INCLUDES - SBR
 *******************************************************************************************************************************************/
#include "./Manager/Manager.h"
#include "BluetoothSerial.h"


// This optional setting causes Encoder to use more optimized code,
// It must be defined before Encoder.h is included.

#include "./BNO080/BNO080.h"



/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

#define SIZE_FILTER 6


#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
// Manager Instance
Manager* manager;

//IMU instance
BNO080 *myIMU;


unsigned long encoder2lastToggled;
bool encoder2Paused = false;

// Task declaration
TaskHandle_t TaskCore0, TaskCore1;

// Timer declaration
hw_timer_t * timer0 = NULL;
hw_timer_t * timer1 = NULL;
hw_timer_t * timer2 = NULL;
hw_timer_t * timer3 = NULL;
portMUX_TYPE timerMux0 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux1 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux2 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux3 = portMUX_INITIALIZER_UNLOCKED;

// Timer Flags
bool flagTimer0 = false;
bool flagTimer1 = false;
bool flagTimer2 = false;
bool flagTimer3 = false;

float TestValue = 0.0f;
char Pressed = '0';

double Roll;
double Pitch;
double Yaw;

int8_t s8Roll;
int8_t s8Pitch;
int8_t s8Yaw;
/*******************************************************************************************************************************************
 *  												TEST
 *******************************************************************************************************************************************/
void test_Add_Requests();
uint32_t test_counter = 0;

bool Delay_1sec = false;
char frame[6]; 
int16_t s16temp = 0;
/*******************************************************************************************************************************************
 *  												CORE LOOPS
 *******************************************************************************************************************************************/
// Loop of core 0
void LoopCore0( void * parameter ){

    while(true) {
        //Serial.println("TEST");
        // Code for Timer 0 interruption
        if (flagTimer0){
            flagTimer0 = false;  
            
            Delay_1sec = true;
         

        }

        // Code for Timer 1 interruption
        if (flagTimer1){
            flagTimer1 = false;
        }

        /****************************************************/
        /*      Manager TASK IMU*/
        /****************************************************/
        if(RC_e::SUCCESS == myIMU->Run()){            
     
            //inputPID = myIMU->m_Pitch;

            Serial.print(F("Roll="));
        	Serial.print(myIMU->m_Roll, 2);
            Serial.print(F("\tPitch="));
            Serial.print(myIMU->m_Pitch, 2);
            Serial.print(F("\t"));
            /*Serial.print(F("Yaw="));
            Serial.print(myIMU->m_Yaw, 2);
            Serial.print(F("\tabsYaw="));
            Serial.print(myIMU->m_absYaw, 2);
            Serial.print(F("\tabsYawCalib="));
            Serial.print(myIMU->m_absYawCalib, 2);
            Serial.print(F("\tyawcalib="));
            Serial.print(myIMU->m_calibYaw, 2);   */         
            Serial.println("\n");
            Roll = myIMU->m_Roll;
            Pitch = myIMU->m_Pitch;
            Yaw = myIMU->m_Yaw;
            s16temp = (int16_t)Roll;

            if(s16temp < -128)
            {
                s8Roll = -128;
            }
            else if (s16temp > 127)
            {
                s8Roll = 127;
            }else{
                s8Roll =(int8_t)s16temp;
            }
            
            s16temp = (int16_t)Pitch;

           if(s16temp < -128)
            {
                s8Pitch = -128;
            }
            else if (s16temp > 127)
            {
                s8Pitch = 127;
            }else{
                s8Pitch =(int8_t)s16temp;
            }

            s16temp = (int16_t)Yaw;

           if(s16temp < -128)
            {
                s8Yaw = -128;
            }
            else if (s16temp > 127)
            {
                s8Yaw = 127;
            }else{
                s8Yaw =(int8_t)s16temp;
            }

            Serial.print(F("Roll="));
        	Serial.print(s8Roll);
            Serial.print(F("\tPitch="));
            Serial.print(s8Pitch);
            Serial.print(F("\t"));
            Serial.println("\n");

            //frame[4] = '\n';
            //frame[5] = '\r';

            /*Serial.print("x");
            Serial.print(s8Roll);
            Serial.print("y");
            Serial.print(s8Pitch);
            Serial.println("\n");*/
            /*conversion from double to String*/

            //Yaw = myIMU->m_Yaw;
            
        } else {
            Serial.println("IMU not connected!!!");
        }

        
        // ========== Code ==========
        if(Serial.available()){
            Serial.println("checking Serial");
            char incomingByte = Serial.read();
            Serial.flush();
            switch (incomingByte)
            {
                case 'w':
                case 'W':
                    Serial.println(" +++++ ESP32 CALIBRATION YAW +++++");
                    myIMU->calibrationAngles();
                    break;
                case 'p':
                case 'P':

                    break;
                case 'r':
                case 'R':
 
                    
                    break;
                case '1':
                    Pressed = '1';
                break;   
                case '2':
                    Pressed = '2';
                break;     
                case '3':
                    Pressed = '3';
                break;      
                case '4':
                    Pressed = '4';
                break;         
            }
        }

        // Run OTA service
        //manager->m_wifiManager->RunOTA();

        // Delay to feed WDT
        delay(10);
    }
}

// Loop of core 1
void LoopCore1( void * parameter ){
    while(true) {
        //Serial.println("TEST");
        // Code for Timer 2 interruption
        if (flagTimer2){
            flagTimer2 = false;

            // ========== Code ==========

            // ==========================
        }

        // Code for Timer 3 interruption
        if (flagTimer3){
            flagTimer3 = false;

            // ========== Code ==========

            // ==========================
        }

            frame[0] = 'x';
            frame[1] = s8Roll;
            frame[2] = 'y';
            frame[3] = s8Pitch;
            frame[4] = 'z';
            frame[5] = s8Yaw;
            SerialBT.printf(frame);
            
            delay(100);
            SerialBT.flush();
            delay(100);
            //SerialBT.print("x");
            
            //delay(30);
            //SerialBT.print(Roll,2);

            //delay(30);
            //SerialBT.print("y");
            
            //delay(30);
            //SerialBT.print(Pitch,2);
            //delay(30);

            //SerialBT.print(Yaw,2);
            //SerialBT.println();
            //delay(50);
        // Delay to feed WDT
        //delay(1);
    }
}

/*******************************************************************************************************************************************
 *  												TIMERS
 *******************************************************************************************************************************************/
// TIMER 0
void IRAM_ATTR onTimer0(){
    portENTER_CRITICAL_ISR(&timerMux0);

    flagTimer0 = true;

    portEXIT_CRITICAL_ISR(&timerMux0);
}

// TIMER 1
void IRAM_ATTR onTimer1(){
    portENTER_CRITICAL_ISR(&timerMux1);

    flagTimer1 = true;

    portEXIT_CRITICAL_ISR(&timerMux1);
}

// TIMER 2
void IRAM_ATTR onTimer2(){
    portENTER_CRITICAL_ISR(&timerMux2);

    flagTimer2 = true;

    portEXIT_CRITICAL_ISR(&timerMux2);
}

// TIMER 3
void IRAM_ATTR onTimer3(){
    portENTER_CRITICAL_ISR(&timerMux3);

    flagTimer3 = true;

    portEXIT_CRITICAL_ISR(&timerMux3);
}

/*******************************************************************************************************************************************
 *  												SETUP
 *******************************************************************************************************************************************/
void setup() {

    // Disable brownout detector
    //WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
    //rtc_wdt_protect_off();
    //rtc_wdt_disable();

    // Serial Port
    Serial.begin(115200);
    Serial.println(" +++++ ESP32 TESTING +++++");

    /*************************************************************************************************/
    SerialBT.begin("ESP32 IMU"); //Bluetooth device name
    /*************************************************************************************************/


    /*****************************************************************************************/
    /* Manager INIT IMU*/
    myIMU = new BNO080();
    myIMU->configure(32, 18, 26, 27, 3000000, 14, 25, 13, DIRECT_ROLL, REVERSE_PITCH, REVERSE_YAW, 18,12);
    myIMU->enableRotationVector(5);
    /*****************************************************************************************/

    // Task of core 0
    xTaskCreatePinnedToCore(
        LoopCore0,  /* Function to implement the task */
        "LoopCore0",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore0, /* Task handle. */
        0);         /* Core where the task should run */

    delay(500);  // needed to start-up task1

    // Task of core 1
    xTaskCreatePinnedToCore(
        LoopCore1,  /* Function to implement the task */
        "LoopCore1",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore1, /* Task handle. */
        1);         /* Core where the task should run */

    // Timer0
    Serial.println("start timer 0");
    timer0 = timerBegin(0, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer0, &onTimer0, true); // edge (not level) triggered 
    timerAlarmWrite(timer0, 5000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer1
    Serial.println("start timer 1");
    timer1 = timerBegin(1, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer1, &onTimer1, true); // edge (not level) triggered 
    timerAlarmWrite(timer1, 2000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer2
    Serial.println("start timer 2");
    timer2 = timerBegin(2, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer2, &onTimer2, true); // edge (not level) triggered 
    timerAlarmWrite(timer2, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer3
    Serial.println("start timer 3");
    timer3 = timerBegin(3, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer3, &onTimer3, true); // edge (not level) triggered 
    timerAlarmWrite(timer3, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Enable the timer alarms
    timerAlarmEnable(timer0); // enable
    //timerAlarmEnable(timer1); // enable
    //timerAlarmEnable(timer2); // enable
    //timerAlarmEnable(timer3); // enable
}

/*******************************************************************************************************************************************
 *  												Main Loop
 *******************************************************************************************************************************************/
// Loop not used
void loop() {
    vTaskDelete(NULL);
}

void test_Add_Requests(){

}


